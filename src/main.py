import pretty_errors

from flask import Flask, render_template, request
from github import Github, Auth

GITHUB_API_URL = "https://api.github.com/search/repositories"
GITHUB_API_TOKEN = ""

token_file = open("./token.txt")
for line in token_file:
    if line != "":
        GITHUB_API_TOKEN = line
        break
token_file.close()

if GITHUB_API_URL != "":
    TOKEN = Auth.Token(GITHUB_API_TOKEN)
else:
    TOKEN = None


app = Flask(__name__, template_folder="./../templates/")


@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        search_query = request.form.get("search_query")
        projects = search_micropython_projects(search_query)
    else:
        projects = []

    return render_template("index.html", projects=projects)


@app.route("/page/<string:usr_name>/<string:repo_name>", methods=["POST", "GET"])
def page(usr_name, repo_name):
    full_repo_name = str(usr_name) + "/" + str(repo_name)
    g = init_github()
    repo = g.search_repositories(full_repo_name).get_page(0)[0]
    g.close()

    author = repo.full_name.split("/")[0]
    stars = repo.stargazers_count
    # readme = markdown(b64decode(repo.get_readme().content))
    downloads = []
    amount_shown = 3
    index = 0

    for download in repo.get_releases():
        tag = repo.get_releases(0).tag_name

        if index == 0:
            downloads.append(["Latest Version " + tag,
                             "https://github.com/" + full_repo_name + "/releases/tag/" + tag])
        else:
            downloads.append(["Version " + download.tag_name])

        if index >= amount_shown:
            break
        index += 1

    return render_template(
        "page.html", project_name=repo.name, author=author,
        stars=stars, readme=readme, downloads=downloads
    )


def search_micropython_projects(query):
    github = init_github()
    search_results = github.search_repositories(
        query=f"{query} topic:micropython")

    projects = []

    try:
        for repo in search_results.get_page(0):

            print(type(repo))

            project = {
                "name": repo.name,
                "owner": repo.owner.login,
                "description": repo.description,
                "stargazers_count": repo.stargazers_count,
                "github_url": repo.svn_url,
                "avatar_url": repo.owner.avatar_url
            }
            projects.append(project)

        return projects
    except ValueError:
        print("Invalid auth token")
        return []


def init_github():
    if TOKEN and True == False:
        return Github(auth=TOKEN)
    else:
        print("No github token available")
        return Github()


def search_repositories(search: str, page: int):
    g = init_github()
    res = []
    for rep in g.search_repositories(search).get_page(page):
        name = rep.full_name
        res.append(name)
    g.close()
    return res


if __name__ == "__main__":
    app.run(debug=True)

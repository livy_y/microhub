# microhub
This is a project that I created because there isn't a centralized database and search engine for MicroPython projects. This serves as a small replacement for such a search engine.

## Getting started
To run the webserver, simply create a virtual environment and install the requirements.txt file using pip. Then, execute the command `flask run --app ./src/main.py`.

## License
This project is under the GNU Affero General Public License.
